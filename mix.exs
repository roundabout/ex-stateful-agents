defmodule StatefulAgents.Mixfile do
  use Mix.Project

  def project do
    [
      app: :stateful_agents,
      version: "0.1.0",
      elixir: "~> 1.5",
      start_permanent: Mix.env == :prod,
      description: description(),
      package: package(),
      deps: deps(),
      source_url: "https://gitlab.com/roundabout/ex-stateful-agents"
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"},
      {:ex_doc, ">= 0.0.0", only: :dev}
    ]
  end

  defp description do
    "This project provides boilerplate modules for using Agents for common
    persistence use-cases. I will add to this over time, as my own needs for
    Agent boilerplate arises."
  end

  defp package do
    [
      files: [
        "lib",
        "lib/stateful_agents.ex",
        "config",
        "config/config.exs",
        "test",
        "test/test_helper.exs",
        "test/stateful_agents_test.exs"
        ],
      licenses: ["WTFPL 2"],
      maintainers: ["David Shepard, daveman1010220@gmail.com"],
      links: %{"Gitlab" => "https://gitlab.com/roundabout/ex-stateful-agents"}
    ]
  end
end
