defmodule StatefulAgentsTest do

  '''
  test "greets the world" do
    assert StatefulAgents.hello() == :world
    refute StatefulAgents.hello() == :foobar
  end
  '''

  defmodule MapAgentTest do
    use ExUnit.Case, async: true
    alias StatefulAgents.MapAgent

    setup do
      {:ok, pid} = MapAgent.start
      {:ok, pid: pid}
    end

    doctest StatefulAgents.MapAgent

    # Negative testing for MapAgent
    test "test that setup started an agent for us to work with", context do
      assert is_pid(context[:pid])
    end

    test "ensure that set fails appropriately when the wrong data type is passed", context do
      assert_raise(ArgumentError, fn -> MapAgent.set(context[:pid], true) end)
    end
  end

  defmodule BooleanAgentTest do
    use ExUnit.Case, async: true
    doctest StatefulAgents.BooleanAgent
  end
end
