# StatefulAgents

This project provides boilerplate modules for using Agents for common
persistence use-cases.

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `stateful_agents` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:stateful_agents, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/stateful_agents](https://hexdocs.pm/stateful_agents).
